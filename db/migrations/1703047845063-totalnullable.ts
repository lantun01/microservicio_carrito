import { MigrationInterface, QueryRunner } from "typeorm";

export class Totalnullable1703047845063 implements MigrationInterface {
    name = 'Totalnullable1703047845063'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "carrito" ADD "ordenesId" text`);
        await queryRunner.query(`ALTER TABLE "carrito" ADD CONSTRAINT "UQ_e7009c83d45cdf3790653e8b371" UNIQUE ("ordenesId")`);
        await queryRunner.query(`ALTER TABLE "carrito" ALTER COLUMN "total" SET DEFAULT '0'`);
        await queryRunner.query(`ALTER TABLE "carrito" ALTER COLUMN "visible" SET DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "carrito" ALTER COLUMN "visible" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "carrito" ALTER COLUMN "total" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "carrito" DROP CONSTRAINT "UQ_e7009c83d45cdf3790653e8b371"`);
        await queryRunner.query(`ALTER TABLE "carrito" DROP COLUMN "ordenesId"`);
    }

}
