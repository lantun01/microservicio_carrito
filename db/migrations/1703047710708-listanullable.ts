import { MigrationInterface, QueryRunner } from "typeorm";

export class Listanullable1703047710708 implements MigrationInterface {
    name = 'Listanullable1703047710708'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "carrito" DROP CONSTRAINT "UQ_e7009c83d45cdf3790653e8b371"`);
        await queryRunner.query(`ALTER TABLE "carrito" DROP COLUMN "ordenesId"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "carrito" ADD "ordenesId" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "carrito" ADD CONSTRAINT "UQ_e7009c83d45cdf3790653e8b371" UNIQUE ("ordenesId")`);
    }

}
