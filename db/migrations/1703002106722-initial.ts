import { MigrationInterface, QueryRunner } from "typeorm";

export class Initial1703002106722 implements MigrationInterface {
    name = 'Initial1703002106722'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "carrito" ("id" SERIAL NOT NULL, "usuarioId" integer NOT NULL, "ordenesId" text NOT NULL, "total" integer NOT NULL, "visible" boolean NOT NULL, "carritoAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_e7009c83d45cdf3790653e8b371" UNIQUE ("ordenesId"), CONSTRAINT "PK_a8af129f65d19017ca8afe737d3" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "carrito"`);
    }

}
