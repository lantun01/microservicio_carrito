import { Module } from '@nestjs/common';
import { CarritoModule } from './carrito/carrito.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from 'db/data-source';

@Module({
  imports: [CarritoModule,TypeOrmModule.forRoot(dataSourceOptions)
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
