import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { CarritoService } from './carrito.service';
import { CreateCarritoDto } from './dto/create-carrito.dto';
import { UpdateCarritoDto } from './dto/update-carrito.dto';

@Controller()
export class CarritoController {
  constructor(private readonly carritoService: CarritoService) {}

  @MessagePattern('createCarrito')
  create(@Payload() createCarritoDto: CreateCarritoDto) {
    return this.carritoService.create(createCarritoDto);
  }

  @MessagePattern('findAllCarrito')
  findAll() {
    return this.carritoService.findAll();
  }

  @MessagePattern('findOneCarrito')
  findOne(@Payload() id: number) {
    return this.carritoService.findOne(id);
  }

  @MessagePattern('updateCarrito')
  update(@Payload() updateCarritoDto: UpdateCarritoDto) {
    return this.carritoService.update(updateCarritoDto);
  }  
}
