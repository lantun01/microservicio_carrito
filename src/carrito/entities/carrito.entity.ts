import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, Timestamp } from "typeorm";

@Entity({name:'carrito'})
export class Carrito {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    usuarioId:number;

    @Column({ type: 'simple-array', unique: true ,nullable: true})
    ordenesId: number[];

    @Column({default: 0})
    total:number;

    @Column({default: true})
    visible:boolean;

    @CreateDateColumn()
    carritoAt:Timestamp;
}
