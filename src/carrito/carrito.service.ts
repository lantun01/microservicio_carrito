import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCarritoDto } from './dto/create-carrito.dto';
import { UpdateCarritoDto } from './dto/update-carrito.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Carrito } from './entities/carrito.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CarritoService {

  constructor(@InjectRepository(Carrito)private readonly CarritoRepository:Repository<Carrito>){}

  async create(createCarrito: CreateCarritoDto):Promise<Carrito>{
    const carrito= this.CarritoRepository.create(createCarrito);
    return await this.CarritoRepository.save(carrito);
  }

  async findAll():Promise<Carrito[]> {
    return await this.CarritoRepository.find();
  }

  async findOne(id: number) {
    const carrito= await this.CarritoRepository.findOne({
      where: {id:id}
    });

    if(!carrito) throw new NotFoundException('orden no encontrado');
    return carrito;
  }

  async update(UpdateCarritoDto: Partial<UpdateCarritoDto>):Promise<Carrito> {
    const new_carrito =await this.findOne(UpdateCarritoDto.id);
    Object.assign(new_carrito,UpdateCarritoDto)
    return await this.CarritoRepository.save(new_carrito);
  }
  
}
