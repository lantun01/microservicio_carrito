import { Column } from "typeorm";

export class CreateCarritoDto {
    @Column()
    usuarioId:number;

    @Column({ type: 'simple-array',nullable: true})
    ordenesId: number[];

    @Column({nullable:true})
    total:number;

    @Column({nullable:true})
    visible:boolean;
}
