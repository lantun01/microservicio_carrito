import { PartialType } from '@nestjs/mapped-types';
import { CreateCarritoDto } from './create-carrito.dto';
import { Carrito } from '../entities/carrito.entity';

export class UpdateCarritoDto extends PartialType(CreateCarritoDto) {
  id: number;
}
